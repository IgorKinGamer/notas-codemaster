package kg

import org.opencv.core.Core
import org.opencv.core.CvType.*
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.highgui.HighGui
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Remove a transparência dos ícones do Material Design.
 * Ícones pretos ficam com fundo branco e vice-versa.
 */
fun main(args: Array<String>)
{
	System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
	
	val caminho = System.getenv("codemaster_icones_md")
	println("caminho = $caminho")
	
	// Acessa os arquivos na pasta
	var coisa = 0
	Files.newDirectoryStream(Paths.get(caminho)).use()
	{
		pasta ->
		for (arquivo in pasta)
		{
			val imagem = abrirImagem(arquivo.toString())
			
			coisa++
			
			//println("imagem.type() = ${imagem.type()}")
			/*println(when(imagem.depth())
			{
				CV_8U  -> "CV_8U "
				CV_8S  -> "CV_8S "
				CV_16U -> "CV_16U"
				CV_16S -> "CV_16S"
				CV_32S -> "CV_32S"
				CV_32F -> "CV_32F"
				CV_64F -> "CV_64F"
				else -> "outro"
			} + "${imagem.channels()}")*/
			
			// Tem transparência
			if (imagem.channels() == 4)
			{
				// Troca transparentes pela cor devida
				for ((i, j) in
						(0 until imagem.rows()) por (0 until imagem.cols()))
				{
					val (b, g, r, a) = imagem[i, j]
					//print("$a ")
					// Cor do ícone, o fundo deve ser o contrário
					val éPreto = arquivo.fileName.toString().contains("black")
					val fundo = if (éPreto) 255.0 else 0.0
					fun cor(original: Double) = (1.0 - a/255.0)*fundo + a/255.0*original
					imagem.put(i, j, cor(b), cor(g), cor(r), 255.0)
				}
				//imagem.convertTo(imagem, CvType.CV_8UC1)
				//println("imagem.channels() = ${imagem.channels()}")
				imagem.apply { Imgproc.cvtColor(this, this, Imgproc.COLOR_BGRA2BGR) }
				//println("imagem.channels() = ${imagem.channels()}")
				//mostrar("$arquivo", imagem)
				//HighGui.moveWindow("$arquivo", 96*coisa, 0)
			}
			
			Imgcodecs.imwrite("$arquivo", imagem)
			
			Imgcodecs.imwrite(
					"$arquivo",
					abrirImagem("$arquivo", Imgcodecs.IMREAD_GRAYSCALE)
			)
			/*if (++coisa == 10)
				break*/
		}
	}
	//esperar()
}

infix fun <T: Any> Iterable<T>.por(colunas: Iterable<T>) = ProdutoCartesiano(this, colunas)
infix fun <T: Any> Iterable<T>.x(colunas: Iterable<T>) = ProdutoCartesiano(this, colunas)

class ProdutoCartesiano<T: Any>(
		private val linhas: Iterable<T>,
		private val colunas: Iterable<T>
): Iterable<Pair<T, T>>
{
	override fun iterator() = object: Iterator<Pair<T, T>>
	{
		/**
		 * Se a próxima chamada a `next()` irá para a próxima linha.
		 * Quando `true`, itColuna já possui um iterador novo.
		 */
		var novaLinha = true
		val itLinha = linhas.iterator()
		lateinit var linha: T
		var itColuna = colunas.iterator()
		
		override fun hasNext(): Boolean
		{
			return if (novaLinha)
				itLinha.hasNext() && itColuna.hasNext()
			else // {!itColuna.hasNext() => novaLinha}, logo, {!novaLinha => itColuna.hasNext()}
				true
		}
		
		override fun next(): Pair<T, T>
		{
			if (novaLinha)
			{
				novaLinha = false
				linha = itLinha.next()
			}
			val coluna = itColuna.next()
			// Acabou a linha
			if (!itColuna.hasNext())
			{
				novaLinha = true
				itColuna = colunas.iterator()
			}
			return Pair(linha, coluna)
		}
		
	}
}