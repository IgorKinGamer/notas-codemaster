## Ideias ##

### Definições ###

Distorção: Imagem com proporção errada (p = α*pOrig ou p = pOrig/α) ou
ampliada demais

Proporção errada

- Não significativa: 1 < α <= 1.1 ou 1.25 (chute)
- Leve (ou média):   1.1 < α <= 1.25 ou 1.5 (também)
- Grave:             α > 1.25 ou 1.5

Ampliada demais

- Não significativa: 1 < α <= 1.1 ou 1.25 (chute)
- Leve (ou média):   1.1 < α <= 1.25 ou 1.5 (também)
- Grave:             α > 1.25 ou 1.5

**Opção 1** (com base na gravidade da distorção e orientação)

- 0 - Há imagens com distorção grave em ambas as orientações
- 1 - Há imagens com distorção em ambas orientações, grave em apenas uma delas
- 2 - Há imagens com distorção no máximo leve ou que ocorre em apenas uma orientação
- 3 - Nenhuma imagem tem distorção significativa

**Opção 2** (com base nos tamanhos usados)

- ...

Analisar apps!

### Cenário ideal ###

Todas as imagens:

- Mantêm a proporção correta
- Não são ampliadas

### Problemas ###

Uma imagem:

- Fica distorcida
	- Quão distorcida?
- Fica ampliada demais
	- Quão ampliada?
- Fica boa em uma orientação, mas apresenta esses problemas na outra
	- Com que gravidade?

### Detecção ###

Como detectar?

Provavelmente, não faz sentido querer tratar cuidadosamente todos os
casos. Em alguns, estaríamos avaliando mais a capacidade matemática do
que o uso correto de imagens.

- "Se minha tela tem proporção 3:4, minha imagem, 5:3, e a largura da
  minha imagem é 50% da tela, a altura tem que ser...?"  
  22,5%! Correto? Sei lá! O que que isso tem a ver com imagens?

Mas, se alguém fizer isso, não merece pontos? ~NÃO~ Pois é...

Bem, em alguns casos não é díficil verificar. Em outros, porém...
Por exemplo, como saber que vai distorcer a imagem porque:

- O texto é muito grande?
- O componente pode ficar "espremido" contra a borda da tela?
- Fill parent não manterá as proporções?

## Fatores ##

Usos corretos "não matemáticos":

- Dimensões em pixels e não grandes demais, respeitando a proporção
	- Sem texto, no caso de componentes que podem ter
	- Ou com texto beeem pequeno, sem risco de distorcer
- Imagem grande com proporções corretas para o fundo, limitando a
  orientação da tela
- Componente Image sem "ScalePictureToFit"

Usos duvidosos:

- Pixels numa dimensão e outra coisa na outra
- Imagem original ruim

Combinações de largura e altura:

|   | A | F | P | % |
|---|---|---|---|---|
| A | o | x | ~ | x |
| F |   | x | x | x |
| P |   |   | o | x |
| % |   |   |   | ~ |

A: Automatic  
F: Fill parent  
P: Pixels  
%: Porcentagem

o: Pode ser bom  
x: Não é uma boa ideia  
~: Pode ser bom em certos casos, explicados abaixo

A-P: Ok em geral se usar o tamanho original, mas é estranho
%-%: Ok se fizer os cálculos para manter a proporção
