package kg

import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.highgui.HighGui
import org.opencv.imgcodecs.Imgcodecs

fun main(args: Array<String>)
{
	System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
	
	//mostrar()
	//misturar()
	//mascarar()
	comparar()
	
	//System.exit(0)
}


fun abrirImagem(caminho: String, tipo: Int = Imgcodecs.IMREAD_UNCHANGED): Mat
{
	val imagem = Imgcodecs.imread(caminho, tipo)
	if (imagem.empty())
		throw Exception("Erro abrindo a imagem \"$caminho\"")
	return imagem
}


fun mostrar(título: String, imagem: Mat) = HighGui.imshow(título, imagem)
fun esperar() = HighGui.waitKey(0)