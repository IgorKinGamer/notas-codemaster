# Componentes com imagem #

- Button
- DatePicker
- Image
- ListPicker
- Screen
- TimePicker

- HorizontalArrangement
- HorizontalScrollArrangement
- VerticalArrangement
- VerticalScrollArrangement

- ImagePicker

- Canvas
- ImageSprite

- Marker

- ContactPicker
- PhoneNumberPicker

## Funcionamento ##

Todos têm:

- Altura (menos Screen)
- Largura (menos Screen)
- Imagem

Altura e largura podem ser:

- Automatic (tamanho "natural")
	- Fica menor se necessário para não sair da tela, em geral
	distorcendo a imagem
- Fill parent
- Pixels (que não são pixels de verdade)
- Porcentagem (da tela) (exceto ImageSprite)

Image:

- Com "Automatic": Tem o tamanho original
	- Quando lagura *e* altura são "Automatic", se uma diminui para não
	sair da tela, a outra diminui proporcionalmente
- Se `"ScalePictureToFit"` for `"True"`, a imagem ocupa todo o espaço
  disponível, possivelmente sendo distorcida.  
  Caso contrário, a proporção original é preservada e a imagem é
  centralizada no espaço disponível.

Screen: A imagem ocupa o fundo da tela inteiro

{*}Arrangement:

- Com "Automatic":
	- Vazio: Tem tamanho padrão independente da imagem
	- Com filhos:
		- Tem no mínimo o tamanho da imagem
		- Fica maior se necessário para acomodar os filhos, distorcendo
		  a imagem

Button, {Date|List|Time|Image|Contact|PhoneNumber}Picker:

- Com "Automatic":
	- Tem, no mínimo, o tamanho da imagem
	- Fica maior se necessário para acomodar o texto, distorcendo a
	  imagem
- Com outros:
	- Respeita a altura
	- Respeita a largura, quebrando o texto e talvez ficando mais alto
	  se a altura for "Automatic"

Canvas:

- Com "Automatic": Tem o tamanho original
- Com outros: Respeita o tamanho escolhido, possivelmente distorcendo a
  imagem

ImageSprite:

- Com "Automatic" ou "Fill parent": Tem o tamanho original
- Com "Pixels": Tem o tamanho escolhido, possivelmente distorcendo a
  imagem

Marker: Parece ignorar o tamanho especificado, tem o tamanho original