package kg

import java.awt.GridLayout
import java.util.Hashtable

import javax.swing.event.ChangeEvent
import javax.swing.event.ChangeListener

import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.highgui.HighGui
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import javax.swing.*

const val tipo = Imgcodecs.IMREAD_GRAYSCALE

fun comparar()
{
	val lista = listOf("arrow_back", "arrow", "pessoa", "cadeira")
	/*for ((i, j) in lista x lista)
		comparar(i, j)*/
	
	for (i in 0..3)
		for (j in i..3)
			comparar(lista[i], lista[j])
	
	println()
	comparar("cadeira", "cadeira", ext2 = "jpg")
	comparar("done", "enod")
	comparar("pessoa", "pessoa2")
	comparar("cadeira", "cadeira2")
	println()
	comparar("pizzaB", "pizzaB")
	comparar("pizzaB", "pizzaP")
	comparar("pizzaB", "pizzaV")
	comparar("pizzaB", "pizzaC")
	comparar("pizzaB", "pizzaHmm")
	comparar("pizzaB", "pizza2")
	println()
	comparar("pizzaP", "pizzaC")
	comparar("pizzaP", "pizzaV")
	comparar("pizzaP", "pizzaHmm")
	comparar("pizzaP", "pizza2")
	println()
	comparar("phone", "meu_telefone")
	
	var nome1: String
	var nome2: String
	while (true)
	{
		nome1 = pedirNome()
		if (nome1 == "") break
		nome2 = pedirNome()
		if (nome2 == "") break
		println("$nome1 × $nome2")
		comparar(nome1, nome2)
	}
	
	//encontrar(imgCena, imgTrecho)
}

fun pedirNome() = JOptionPane.showInputDialog("Nome:")!!

fun comparar(nome1: String, nome2: String, t: Int = tipo, ext1: String = "png", ext2: String = "png") =
		calcularSimilaridade(
				abrirImagem("rec/img/$nome1.$ext1", t),
				abrirImagem("rec/img/$nome2.$ext2", t)
		) //.also { println("($nome1, $nome2)") }

fun calcularSimilaridade(referência: Mat, original: Mat): Double
{
	// Redimensiona para o tamanho da imagem de referência
	val imagem = Mat()
			.apply { Imgproc.resize(original, this, referência.size()) }
	
	/* matchTemplate() */
	for (algoritmo in listOf(
			/*Imgproc.TM_CCOEFF,*/ Imgproc.TM_CCOEFF_NORMED,
			/*Imgproc.TM_CCORR,*/ Imgproc.TM_CCORR_NORMED,
			/*Imgproc.TM_SQDIFF,*/ Imgproc.TM_SQDIFF_NORMED
	))
	{
		// Compara
		val resultado = Mat()
				.apply()
				{
					Imgproc.matchTemplate(
							referência, imagem,
							this,
							algoritmo)
				}
		
		print( String.format(
				when (algoritmo)
				{
					Imgproc.TM_CCOEFF, Imgproc.TM_CCORR, Imgproc.TM_SQDIFF ->
							"%15.2f"
					else ->
							"%15.6f"
				},
				resultado[0, 0][0]
		) )
	}
	//println()
	print("  --/--  ")
	
	/* matchShapes() */
	for (modo in listOf(
			Imgproc.CONTOURS_MATCH_I1, Imgproc.CONTOURS_MATCH_I2, Imgproc.CONTOURS_MATCH_I3
	))
	{
		val resultado = Imgproc.matchShapes(referência, imagem, modo, 0.0)
		print(String.format("%.5f ", resultado))
	}
	println()
	
	// TODO Escolher qual valor retornar
	return 0.0
}


/**
 * Exemplo (template matching) levemente alterado.
 */
internal class MatchTemplateDemo : ChangeListener
{
	
	//! [declare]
	/// Global Variables
	var use_mask: Boolean = false
	var img = Mat()
	var templ = Mat()
	var mask = Mat()
	
	var match_method: Int = 0
	
	var imgDisplay = JLabel()
	var resultDisplay = JLabel()
	//! [declare]
	
	fun run(args: Array<String>)
	{
		//! [load_image]
		/// Load image and template
		img = abrirImagem("rec/img/cena.png", Imgcodecs.IMREAD_COLOR)
		templ = abrirImagem("rec/img/trecho.png", Imgcodecs.IMREAD_COLOR)
		//! [load_image]
		
		if (args.size > 2)
		{
			use_mask = true
			mask = Imgcodecs.imread(args[2], Imgcodecs.IMREAD_COLOR)
		}
		
		matchingMethod()
		createJFrame()
	}
	
	private fun matchingMethod()
	{
		val result = Mat()
		
		//! [copy_source]
		/// Source image to display
		val img_display = Mat() .apply { img.copyTo(this) }
		//! [copy_source]
		
		//! [match_template]
		/// Do the Matching and Normalize
		val method_accepts_mask = Imgproc.TM_SQDIFF == match_method || match_method == Imgproc.TM_CCORR_NORMED
		if (use_mask && method_accepts_mask)
			Imgproc.matchTemplate(img, templ, result, match_method, mask)
		else
			Imgproc.matchTemplate(img, templ, result, match_method)
		//! [match_template]
		
		//! [normalize]
		Core.normalize(result, result, 0.0, 1.0, Core.NORM_MINMAX, -1, Mat())
		//! [normalize]
		
		//! [best_match]
		/// Localizing the best match with minMaxLoc
		val mmr = Core.minMaxLoc(result)
		//! [best_match]
		
		//! [match_loc]
		/// For SQDIFF and SQDIFF_NORMED, the best matches are lower values.
		/// For all the other methods, the higher the better
		val matchLoc =
				when (match_method)
				{
					Imgproc.TM_SQDIFF, Imgproc.TM_SQDIFF_NORMED ->
						mmr.minLoc
					else ->
						mmr.maxLoc
				}
		//! [match_loc]
		
		//! [imshow]
		/// Show me what you got
		Imgproc.rectangle(img_display,
				matchLoc, Point(matchLoc.x + templ.width(), matchLoc.y + templ.height()),
				Scalar(0.0, 0.0, 0.0), 2, 8, 0)
		Imgproc.rectangle(result,
				matchLoc, Point(matchLoc.x + templ.width(), matchLoc.y + templ.height()),
				Scalar(0.0, 0.0, 0.0), 2, 8, 0)
		
		var tmpImg = HighGui.toBufferedImage(img_display)
		var icon = ImageIcon(tmpImg)
		imgDisplay.icon = icon
		
		result.convertTo(result, CvType.CV_8UC1, 255.0)
		tmpImg = HighGui.toBufferedImage(result)
		icon = ImageIcon(tmpImg)
		resultDisplay.icon = icon
		//! [imshow]
	}
	
	override fun stateChanged(e: ChangeEvent)
	{
		val source = e.source as JSlider
		if (!source.valueIsAdjusting)
		{
			match_method = source.value
			matchingMethod()
		}
	}
	
	private fun createJFrame()
	{
		val title = "Source image; Control; Result image"
		val frame = JFrame(title)
		frame.layout = GridLayout(2, 2)
		frame.add(imgDisplay)
		
		//! [create_trackbar]
		val min = 0
		val max = 5
		val slider = JSlider(JSlider.VERTICAL, min, max, match_method)
		//! [create_trackbar]
		
		slider.paintTicks = true
		slider.paintLabels = true
		
		// Set the spacing for the minor tick mark
		slider.minorTickSpacing = 1
		
		// Customizing the labels
		val labelTable = Hashtable<Int, JLabel>()
		labelTable[0] = JLabel("0 - SQDIFF")
		labelTable[1] = JLabel("1 - SQDIFF NORMED")
		labelTable[2] = JLabel("2 - TM CCORR")
		labelTable[3] = JLabel("3 - TM CCORR NORMED")
		labelTable[4] = JLabel("4 - TM COEFF")
		labelTable[5] = JLabel("5 - TM COEFF NORMED : (Method)")
		slider.labelTable = labelTable
		
		slider.addChangeListener(this)
		
		frame.add(slider)
		
		frame.add(resultDisplay)
		frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
		frame.pack()
		frame.isVisible = true
	}
}

fun main(args: Array<String>)
{
	// load the native OpenCV library
	System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
	System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
	
	// run code
	MatchTemplateDemo().run(args)
}


/**
 * Procura a posição em `imagem` mais parecida com `subimagem`.
 * Versão simplificada do exemplo acima.
 */
fun encontrar(imagem: Mat, subimagem: Mat)
{
	for (algoritmo in listOf(
			Imgproc.TM_CCOEFF, Imgproc.TM_CCOEFF_NORMED,
			Imgproc.TM_CCORR, Imgproc.TM_CCORR_NORMED,
			Imgproc.TM_SQDIFF, Imgproc.TM_SQDIFF_NORMED
	))
	{
		// Procura
		val resultado = Mat()
				.apply()
				{
					Imgproc.matchTemplate(
							imagem, subimagem,
							this,
							algoritmo)
					Core.normalize(
							this, this,
							0.0, 1.0,
							Core.NORM_MINMAX, -1, Mat()
					)
					convertTo(this, CvType.CV_8UC1, 255.0)
				}
		val ponto =
				when (algoritmo)
				{
					Imgproc.TM_SQDIFF, Imgproc.TM_SQDIFF_NORMED ->
						Core.minMaxLoc(resultado).minLoc
					else ->
						Core.minMaxLoc(resultado).maxLoc
				}
		Imgproc.rectangle(
				resultado,
				ponto,
				Point(ponto.x + subimagem.cols(), ponto.y + subimagem.rows()),
				Scalar(0.0, 0.0, 0.0), 2, 8, 0)
		
		mostrar("$algoritmo", resultado)
		HighGui.moveWindow("$algoritmo",
				imagem.width() * (algoritmo/2),
				imagem.height() * (algoritmo%2))
	}
	
	esperar()
}
