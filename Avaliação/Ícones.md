## Detectar ícones ##

A função `Imgproc.matchTemplate()` parece servir para detectar se uma
imagem é um ícone do Material Design.

O propósito dessa função é encontrar, em uma imagem, a porção mais
parecida com uma outra imagem (subimagem).
Porém, usando uma "subimagem" de tamanho igual ao da imagem, está-se
calculando simplesmente quão parecidas as imagens são.

Outra função, `Imgproc.matchShapes()`, não deu resultados interessantes.

### Resultados de testes ###

Duas condições *simultaneamente*:

- Algoritmo CCOEFF_NORM >= 0.6 (Valor no intervalo [-1, 1])
- Algoritmo CCORR_NORM >= 0.9 (Valor no intervalo [0, 1])

Comparações entre imagens da pasta Critérios/rec/img/:

- Ambas condições satisfeitas:
	- «x» × «x» (qualquer consigo mesma): Ambos dão ~1.0
		- Ambos servem se a imagem é exatamente a mesma
		- Com leve ruído (cadeira.png × cadeira.jpg), ainda dão > 0.999
	- pessoa × pessoa2
		- Pequena diferença (mas ambos são do MD), resultado justo.
	- phone × meu_telefone
		- Ambos passam por pouco (0.606, 0.925).
		  São consideravelmente parecidas: meu_telefone é um desenho mal
		  feito, mas parecido.
	- pizzaB × pizza{V,C}
		- Só o que muda é a cor (lembrando que tudo está sendo carregado
		  em tons de cinza), a cor de fundo é a mesma. Ambos > 0.99.
	- pizzaP × pizza{Hmm,2}
		- "pizzaHmm" muda todas as cores, pizza2 é um tipo diferente de
		  ícone também do MD.
		  Os resultados ficaram dentro dos limites com uma boa margem.

- Só CCORR_NORM satisfeita:
	- done × enod
		- Pode-se subjetivamente dizer que são parecidas (espelhadas
		  horizontalmente), mas "enod" já não é um ícone do MD...
		  O CCOEFF_NORM se mostrou sensível a esse tipo de diferença.
	- arrow[_back] × {pessoa,cadeira}
		- Nada a ver, mas CCORR_NORM satisfaz (ou quase).
		  CCOEFF_NORM fica longe.
	- cadeira × cadeira2
		- CCOEFF_NORM fica na beirada (0.592), CCORR_NORM satisfaz por
		  pouco (0.926).
		  Os dois ficaram +- no limite.
		  Não é a mesma imagem, mas são bem parecidas, então tudo bem...

- Só CCOEFF_NORM satisfeita:
	- Nenhum caso encontrado.

- Nenhum dos dois satisfeito:
	- «x»P × «x»B (um ícone na versão preta × na versão branca)
