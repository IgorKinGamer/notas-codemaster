package kg

import java.io.File
import javax.imageio.ImageIO
import javax.imageio.ImageReader
import javax.swing.JOptionPane

/**
 * Usa a API do Java para descobrir as dimensões de uma imagem
 */
fun main(args: Array<String>)
{
	// Nome do arquivo da imagem
	val nome = JOptionPane.showInputDialog("Arquivo (imagem)")
	println("Arquivo: $nome")
	
	// Abre a imagem
	val arq = File(nome)
	val stream = ImageIO.createImageInputStream(arq);
	
	// Lista e mostra quais leitores existem
	val leitores = mutableListOf<ImageReader>()
	for (leitor in ImageIO.getImageReaders(stream))
	{
		println(leitor)
		leitores.add(leitor)
	}
	
	// Usa o primeiro
	leitores[0].run()
	{
		// De onde a imagem será lida
		input = stream
		
		// Mostra o número de imagens
		println(getNumImages(true))
		
		// Mostra dados
		println(
				  "Largura:   ${getWidth(0)}\n"
				+ "Altura:    ${getHeight(0)}\n"
				+ "Proporção: ${getAspectRatio(0)}"
		)
	}
	
}
