# Implementação #

## Ícones ##

Ícones utilizados no aplicativo provêm do Material Design.

### Notas ###

- Ícones no App Inventor são componentes que têm imagem *e* são alvo de toque
(Botão, EscolheContato, EscolheData, EscolheEmail, EscolheHora, EscolheImagem, EscolheLista, EscolheNumeroDeTelefone, SpriteImagem, Pintura)


## Imagens ##

Imagens precisam ter uma resolução/proporção mínima de qualidade, evitando pixelização ou distorções.

### Problemas ###

Definições:

	Proporção:          p     = «largura» / «altura»
	Proporção original: pOrig = «largura original» / «altura original»
	Ampliação:
		a = max(
			«largura» / «largura original»,
			«altura» / «altura original»
		)

- Distorcida: proporções erradas
	- Correto: p = pOrig
	- Errado: p = α*pOrig ou p = pOrig/α (α > 1)  
	  Ou seja, respectivamente, α vezes mais larga ou mais alta do que
	  o correto.
	  Quanto maior o α, pior.
	- Indeterminado: a largura ou altura se adapta à tela.
	  Pode ficar correto ou quase, bem como pode ficar horrível.
		- Dependendo das dimensões da imagem, dos tamanhos "comuns" de
		  tela e da direção do celular (horizontal/vertical), poderia se
		  inferir aproximadamente o α.  
		  Dá pra controlar a direção do layout no App Inventor?  
		  Dá, SIM! `Screen.ScreenOrientation`.
		- Solução simples: considerar como ruim e pronto

- Pixelizada ou desfocada/borrada: imagem possivelmente ampliada demais
	- Imagem originalmente com resolução "perfeita"
		- Correto: a <= 1 (tamanho original ou menor)
		- Errado: a > 1 (ampliado)
		- Pode haver uma margem de tolerância (1.??? em vez de 1)
	- Imagem originalmente com resolução ruim
		- Pode ter um "fator de distorção" f, que diz quão ruim a imagem
		  é originalmente. Assim, usaria-se a*f no lugar de a.
		- Como reconhecer que a resolução é ruim?
			- Bordas pouco definidas
				- Lógica fuzzy?
		- Solução simples: Ignorar a resolução original

### Ferramentas ###

#### API do java ####

Formatos obrigatoriamente suportados: JPEG, PNG, BMP, WBMP, GIF
([link](https://docs.oracle.com/javase/8/docs/api/javax/imageio/package-summary.html))

Formatos do Android: BMP, GIF, JPEG, PNG, WebP, HEIF
([link](https://developer.android.com/guide/topics/media/media-formats#image-formats))

`ImageIO.getImageReaders[By*]()`

Classe `ImageReader`

- `getWidth()`
- `getHeight()`
- `getAspectRatio()`

Usar esses métodos funcionou.
